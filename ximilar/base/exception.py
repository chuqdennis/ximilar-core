

class DeepException(Exception):
    def __init__(self, code, msg=None):
        Exception.__init__(self, msg)
        self.code = code
        self.msg = msg

    def simple_msg(self):
        return ERRMSG[self.code]

    def full_msg(self):
        return ERRMSG[self.code] + ':' + self.msg

    def __str__(self):
        return ERRMSG[self.code]


class ERRCODE(object):
    OK = 0
    DEFAULT = 1
    CELERY = 2
    NOTINSTANCE = 3
    PRECONDITION = 4
    CONTRECORD = 5
    IOERROR = 6
    EXTRACTION_ERROR = 7

ERRMSG = {
    ERRCODE.OK: "Everything is OK",
    ERRCODE.DEFAULT: "Unexpected error in web service.",
    ERRCODE.CELERY: "Unexpected error from celery service.",
    ERRCODE.NOTINSTANCE: "Passed object is not demanded instance.",
    ERRCODE.PRECONDITION: "Precondition is not satisfied.",
    ERRCODE.CONTRECORD: "The request must contain either field %s.",
    ERRCODE.IOERROR: "Unexpected io error.",
    ERRCODE.EXTRACTION_ERROR: "Unexpected error when using extraction",
}

