import time
from ximilar.base.record_checker import RecordChecker
from ximilar.api.python_api import Record


class DataProcessor(RecordChecker):
    """
    This is a base class for processors that take an array of "data records" and modify them somehow
    (e.g. by adding image, descriptor, ...).
    """
    def process_records(self, json_records):
        """
        Given an array of JSON "data records", this method should modify all records
        :param json_records: an array of python dictionaries (JSON objects/maps) as returned by JSON.parse
        :return: an array of modified records
        :rtype: list
        :raise Exception if the processing fails
        """
        raise NotImplementedError


def timeit_process_records(method):
    """
    If we want to measure time of process_records(DataProcessor) then class need to have:
        Logger object
        timeit==Logger.debug param
        name param!

    :param method: method of object to measure
    :return: modified records
    """
    def timed(*args, **kw):
        start_time = time.time()
        records = method(*args, **kw)
        finish_time = float(time.time() - start_time)

        if args[0].timeit:
            for i in range(len(records)):
                if Record.TIME_STATS in records[i]:
                    records[i][Record.TIME_STATS][str(args[0])] = finish_time
                else:
                    records[i][Record.TIME_STATS] = {str(args[0]): finish_time}
        return records
    return timed
