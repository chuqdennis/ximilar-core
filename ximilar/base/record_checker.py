import abc

from ximilar.api.common_api import Response
from ximilar.api.common_api import Record


class RecordChecker(object, metaclass=abc.ABCMeta):
    """
    This is a base abstract class for any kinds of processors that take JSON records and check that the record is valid
    (it follows some condition)
    """

    def pre_condition(self, json_record):
        """
        Checks that the record is processable by this processor.
        :param json_record: JSON object (map) as returned by JSON.parse
        :return: a pair (True/False, message), where the message says what was wrong with the record
        """
        return Record.REC_STATUS not in json_record \
               or json_record[Record.REC_STATUS][Response.STATUS_CODE] == Response.STATUS_CODE_OK

    @abc.abstractmethod
    def pre_condition_desc(self):
        """
        Gets the description of what the record must fulfill to be valid for this processor.
        :return: a string message saying what is checked by the pre_condition method
        """
        raise NotImplementedError

    @abc.abstractmethod
    def get_modified_fields(self):
        """
        Returns an array of string names of the JSON fields that are to be modified by this class.
        :return: an array of strings
        """
        raise NotImplementedError
