import os

from ximilar.api.common_api import Response
from ximilar.api.python_api import Record
from ximilar.base.data_processor import DataProcessor
from ximilar.base.config import ArgumentParser


class DataCleaner(DataProcessor):
    """
    This is a class for cleaning data for responses. It is used mostly as end processor in chain.
    For example it clean the Record.IMAGE_DATA field from json dictionary, ...
    """
    @staticmethod
    def get_arg_parser(name="DataCleaner"):
        parser = ArgumentParser(name)
        parser.add_argument("--clean_field", default=[], type=list, help='additional field(s) to delete')
        parser.add_argument("--clean_cached_file", default=False, type=bool, help='Remove cached files from storage')
        return parser

    def __init__(self, args, name="DataCleaner"):
        self.additional_fields = args[name].clean_field
        self.clean_cached_file = args[name].clean_cached_file
        self.basic_fields = [Record.IMG_DATA, Record.PIL_IMG_DATA, Record.BASE64, Record.VIDEO_DATA, Record.SETTINGS]

    def __str__(self):
        return "DataCleaner"

    def pre_condition(self, json_record):
        return True

    def pre_condition_desc(self):
        return 'DataCleaner: True Always even when there are errors in json_records'

    def get_modified_fields(self):
        return [Record.IMG_DATA, Record.BASE64, Record.PIL_IMG_DATA]

    def process_records(self, json_records):
        for i in range(len(json_records)):
            try:
                json_records[i] = self.clean_data(json_records[i])
            except Exception as e:
                json_records[i][Response.STATUS][Response.STATUS_CODE] = 500
                json_records[i][Response.STATUS][Response.STATUS_TEXT] = str(e)

        return json_records

    def clean_data(self, json_record):
        """
        Clean data from one json_record. This method can clean basic data fields
        as numpy data of loaded image/video, base64(because it can be very big),
        settings, ...
        :param json_record: json record with internal data
        :return: cleaner json record which we return as response
        """
        if self.clean_cached_file and Record.CACHED_FILE in json_record and os.path.isfile(json_record[Record.CACHED_FILE]):
            try:
                os.remove(json_record[Record.CACHED_FILE])
                del json_record[Record.CACHED_FILE]
            except:
                pass

        for clean_key in self.basic_fields:
            if clean_key in json_record:
                 del json_record[clean_key]

        for clean_key in self.additional_fields:
            if clean_key in json_record:
                 del json_record[clean_key]

        return json_record
