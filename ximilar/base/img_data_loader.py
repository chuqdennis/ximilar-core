import base64
import cv2
import numpy as np
import os
import os.path
import re
import urllib.request, urllib.parse, urllib.error
import random
import string
import concurrent.futures


from ximilar.base.logger import Logger
from ximilar.api.common_api import Response
from ximilar.api.python_api import Record
from ximilar.base.config import ArgumentParser
from ximilar.base.exception import DeepException, ERRCODE
from ximilar.base.data_processor import DataProcessor, timeit_process_records


class ImgDataLoader(DataProcessor):
    """
    This is a class for (downloading and) reading images and post processing them
    It works with json and extend json with image data.
    """
    numbers = re.compile('\d+(?:\.\d+)?')

    RGB_SPACE = "RGB"
    BGR_SPACE = "BGR"
    HSV_SPACE = "HSV"
    LUV_SPACE = "LUV"

    @staticmethod
    def get_arg_parser(name="ImgDataLoader"):
        parser = ArgumentParser(name=name, parents=[ImgResizer.get_arg_parser(name=name)])
        parser.add_argument("--color_space",
                            help="Set the result color space for image [RGB, BGR, HSV, LUV](Default RGB)",
                            default="RGB")
        parser.add_argument("--save_img_folder",
                            help="If this is set, it will store picture with _url or _base64 in specific folder.",
                            default="")
        parser.add_argument("--cache_folder", help="Which folder should be cached", default="")
        parser.add_argument("--save_quite", help="if saving is quite", type=bool, default=False)
        parser.add_argument("--threads", help="number of threads for loading imgs", type=int, default=3)
        return parser

    def __init__(self, args, name="ImgDataLoader", img_field=Record.IMG_DATA):
        self.name = name
        self.logger = Logger(args, name=name)
        self.timeit = self.logger.log_debug

        self.resizer = ImgResizer(args, name=name)
        self.color_space = args[name].color_space.upper()
        self.save_img_folder = args[name].save_img_folder
        self.cache_folder = args[name].cache_folder
        self.save_quite = args[name].save_quite
        self.img_field = img_field
        self.executor = concurrent.futures.ThreadPoolExecutor(max_workers=args[name].threads)

    def __str__(self):
        return self.name

    @timeit_process_records
    def process_records(self, json_record):
        """
        Load images in parallel with thread pool.
        Images can be loaded from _url, _file or _base64.
        We are using some blocking operation thatt is why we are not using asyncio here.
        :param json_record: list of JSON records with fields '_url' or '_file' or '_base64'
        :return: modified json record with field '_img_data'
        """
        futures = [self.executor.submit(self.process_record, record) for record in json_record]
        results = [future.result() for future in futures]
        return results

    def process_record(self, json_record):
        """
        Load one json record image, store it if necessary and resize it.
        """
        try:
            image, height, width = self.get_image(json_record)
            json_record[self.img_field] = image

            if Record.WIDTH not in json_record:
                # WIDTH AND HEIGHT ARE ALWAYS BASED ON ORIGINAL IMAGE!
                json_record[Record.WIDTH] = width
                json_record[Record.HEIGHT] = height

            # save the original image
            json_record = self.save_image(json_record, self.save_img_folder, field=Record.FILE)

            # final resizing, getting object and more ...
            json_record = self.resizer.process_record(json_record)

            # cache the image after resizing
            json_record = self.save_image(json_record, self.cache_folder, field=Record.CACHED_FILE)
        except Exception as e:
            self.logger.sentry_exception()
            json_record[Record.REC_STATUS][Response.STATUS_CODE] = Response.STATUS_CODE_ERROR
            json_record[Record.REC_STATUS][Response.STATUS_TEXT] = "Error Loading Image:" + str(e)

        return json_record

    def save_image(self, json_record, folder, field=Record.FILE):
        if len(folder) > 0 and field not in json_record:
            filename = self.store_image(json_record, folder)
            if filename and not self.save_quite:
                json_record[field] = filename
        return json_record

    def pre_condition(self, json_record):
        return super(ImgDataLoader, self).pre_condition(json_record)\
               and (Record.URL in json_record or Record.FILE in json_record or Record.BASE64 in json_record)

    def pre_condition_desc(self):
        return 'the record must contain either field \'' + Record.URL + '\' or field \'' + Record.FILE \
               + '\'' + 'or field \'' + Record.BASE64 + '\''

    def get_modified_fields(self):
        return [self.img_field, Record.WIDTH, Record.HEIGHT]

    def store_image(self, json_record, folder):
        """
        Store Image if it is in Record.URL or in Record.BASE64
        :param json_record: one json record with image data and other fields
        :return: image filename on disk
        """
        filename = ImgDataLoader.generate_file_name(folder)
        cv2.imwrite(filename, self.convert_color_space_back(json_record[self.img_field]))
        return filename

    def get_image(self, json_record):
        """
        This method reads an image file and returns it in such a format and size that is accepted by neural base.
        If attribute '_file' specified and given local path exists, this file is read.
        If attribute '_url' specified this file is downloaded from given url.
        If attribute '_base64' this file is converted from base64 representation.
        If nothing works, None is returned.
        :param pre_process_data:
        :param json_record: JSON record expecting to have at least one of '_file' or '_url'
        :return: image as read by cv2 (and resized) or None
        :raise Exception if the image donwload fails
        """
        image, height, width = None, None, None

        if self.img_field in json_record:
            return json_record[self.img_field], json_record[Record.HEIGHT], json_record[Record.WIDTH]

        # the cache image is loaded only if we already have original image size
        if Record.CACHED_FILE in json_record and Record.WIDTH in json_record:
            image = self.load_file(json_record, json_record[Record.CACHED_FILE])

        # otherwise try to load the file in common way
        if image is None and Record.BASE64 in json_record:
            image = self.get_image_base64(json_record)
        if image is None and Record.FILE in json_record:
            image = self.load_file(json_record, json_record[Record.FILE])
        if image is None and Record.URL in json_record:
            image = self.get_image_url(json_record)

        # convert image to rgb
        if image is not None:
            image, height, width = self.post_process_input_data(image)
        else:
            raise Exception('Error loading data for this record')

        return image, height, width

    def load_file(self, json_record, path):
        try:
            image = self.get_image_file(path)
        except Exception as e:
                raise e
        return image

    def get_image_base64(self, json_record):
        """
        Convert base64 data to image.
        :param json_record: json with attribute '_base64'
        :return: opencv2/numpy image
        """
        try:
            img_array = base64.b64decode(json_record[Record.BASE64])
            image = np.fromstring(img_array, dtype=np.uint8)
            image = cv2.imdecode(image, 1)
            if image.shape[2] != 3:
                raise Exception("ImgDataLoader: Image has not shape (height, width, 3)")
            return image
        except Exception as e:
            self.logger.sentry_exception()
            raise Exception("ImgDataLoader: Error reading base64:" + str(e))

    def get_image_file(self, path):
        """
        Load image from disk.
        :param path: path to the file
        :return: opencv2/numpy image or None, if there was an error reading of the file
        """
        if not os.path.isfile(path):
            raise Exception("ImgDataLoader: File '" + path + "' does not exist!")
        try:
            image = cv2.imread(str(path))
            return image
        except Exception:
            raise Exception("ImgDataLoader: Error reading file " + path + ".")

    def get_image_url(self, json_record, attempt=0):
        """
        Download image from url.
        :param json_record: json with attribute '_url'
        :return: opencv2/numpy image
        """
        try:
            resp = urllib.request.urlopen(str(json_record[Record.URL]))
            img_bin = resp.read()
            image = np.asarray(bytearray(img_bin), dtype="uint8")
            image = cv2.imdecode(image, cv2.IMREAD_COLOR)

            if image is None:
                raise Exception("ImgDataLoader: Error extracting image from '"
                                    + str(json_record[Record.URL]) + "'")
            return image
        except Exception:
            # Three attempts to download file
            if attempt > 1:
                self.logger.sentry_exception()
                raise Exception("ImgDataLoader: Error downloading image file from '" + str(
                                    json_record[Record.URL]) + "'")
            else:
                return self.get_image_url(json_record, attempt+1)

    @staticmethod
    def generate_file_name(save_img_folder):
        return save_img_folder + '/' + ImgDataLoader._generate_directory(save_img_folder) + '/' + ''.join(
            random.SystemRandom().choice(string.ascii_uppercase + string.digits + string.ascii_lowercase) for _ in
            range(30)) + '.jpg'

    @staticmethod
    def _generate_directory(save_img_folder):
        directory = str(random.randint(0, 999))
        if not os.path.exists(save_img_folder + '/' + directory):
            os.makedirs(save_img_folder + '/' + directory)
        return directory

    def convert_color_space(self, image):
        if self.color_space == ImgDataLoader.RGB_SPACE:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        elif self.color_space == ImgDataLoader.HSV_SPACE:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        elif self.color_space == ImgDataLoader.LUV_SPACE:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2LUV)
        return image

    def convert_color_space_back(self, image):
        if self.color_space == ImgDataLoader.RGB_SPACE:
            image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
        elif self.color_space == ImgDataLoader.HSV_SPACE:
            image = cv2.cvtColor(image, cv2.COLOR_HSV2BGR)
        elif self.color_space == ImgDataLoader.LUV_SPACE:
            image = cv2.cvtColor(image, cv2.COLOR_LUV2BGR)
        return image

    def post_process_input_data(self, image):
        """
        Resizes the image, if necessary, and returns a triple: image, height, width
        :param image: the OpenCV image
        :return: a triple: (image, height, width)
        """
        try:
            height, width, _ = image.shape
            return self.convert_color_space(image), height, width
        except Exception:
            raise DeepException(ERRCODE.IOERROR, "ImgDataLoader: Problem with resizing or converting image.")


class ImgResizer(DataProcessor):
    """
    This is a class which takes an already opened image and resizes it, if necessary.
    """
    @staticmethod
    def get_arg_parser(name="ImgResizer"):
        parser = ArgumentParser(name)
        parser.add_argument("--resize", help="if zero then no resize else use this number", type=int, default=224)
        parser.add_argument("--preserve_aspect_ratio", help="if preserving aspect ratio", default=False, type=bool)
        parser.add_argument("--get_object", help="get data of object if it is present", default=False, type=bool)
        return parser

    def __init__(self, args, name="ImgResizer"):
        self.img_size = int(args[name].resize)
        self.preserve_aspect_ratio = args[name].preserve_aspect_ratio
        self.get_object = args[name].get_object

    def __str__(self):
        return "ImgResizer"

    def pre_condition(self, json_record):
        return Record.IMG_DATA in json_record

    def pre_condition_desc(self):
        return 'the record must contain fields \'' + Record.IMG_DATA + '\''

    def get_modified_fields(self):
        return [Record.IMG_DATA, Record.WIDTH, Record.HEIGHT]

    def process_records(self, json_records):
        """
        Resizes every image, if necessary.
        :param json_records: list of JSON records with fields '_img_data'
        :return: json records with modified field '_img_data'
        """
        for i in range(len(json_records)):
            try:
                json_records[i] = self.process_record(json_records[i])
            except Exception as e:
                json_records[i][Record.REC_STATUS][Response.STATUS_CODE] = Response.STATUS_CODE_ERROR
                json_records[i][Record.REC_STATUS][Response.STATUS_TEXT] = "Error resizing image: " + str(e)
        return json_records

    def process_record(self, json_record):
        json_record[Record.IMG_DATA] = self.extract_object(json_record[Record.IMG_DATA], json_record)
        json_record = self.resize(json_record, self.img_size)
        return json_record

    def extract_object(self, image, json_record):
        """
        If the "_objects" is present in the record than take random object from this image.
        :param image: image from cv2
        :param json_record: json dictionary with possible object field
        :return: if object field is present then cut the object from image and return it
        """
        if Record.OBJECTS in json_record and self.get_object:
           if len(json_record[Record.OBJECTS]) > 0:
               pick = random.randint(0, len(json_record[Record.OBJECTS])-1)
               if Record.BOUNDING_BOX in json_record[Record.OBJECTS][pick]:
                   bbox = json_record[Record.OBJECTS][pick][Record.BOUNDING_BOX]
                   image = image[int(bbox[0]):int(bbox[2]), int(bbox[1]):int(bbox[3])]
                   return image
        return image

    def resize(self, json_record, img_size):
        try:
            if Record.IMG_DATA not in json_record:
                return json_record
            image = json_record[Record.IMG_DATA]
            height, width, _ = image.shape
            if int(img_size) > 1 and not (img_size == height and img_size == width):
                dim = self.get_aspect_ratio_dim(image, img_size, self.preserve_aspect_ratio)

                # dims[1] is height and dims[0] is width in cv2 resize
                if height > dim[1] or width > dim[0]:
                    image = cv2.resize(image, dim, interpolation=cv2.INTER_AREA).astype(np.uint8)
                else:
                    image = cv2.resize(image, dim).astype(np.uint8)
                json_record[Record.IMG_DATA] = image
            return json_record
        except Exception:
            raise DeepException(ERRCODE.IOERROR, "ImgDataLoader: Problem with resizing image.")

    @staticmethod
    def get_aspect_ratio_dim(image, img_size, preserve_aspect_ratio):
        if preserve_aspect_ratio:
            if image.shape[0] > image.shape[1]:
                r = float(img_size) / image.shape[1]
                dim = (img_size, int(image.shape[0] * r))
            else:
                r = float(img_size) / image.shape[0]
                dim = (int(image.shape[1] * r), img_size)
            return dim
        return img_size, img_size
