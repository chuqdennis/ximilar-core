import json
import os
import re
import yaml


class ArgumentArgs:
    def __init__(self, dictionary):
        for k, v in list(dictionary.items()):
            setattr(self, k, v)


class ArgumentParser(object):
    """
    This is our YAML configuration parser.
    """

    def __init__(self, name=None, description="", parents=[]):
        self.parents = parents
        self._name = name
        self.description = description
        self.args_to_parse = {}

        for parent in parents:
            if self._name:
                parent.name = name

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name
        for parent in self.parents:
            if self._name:
                parent.name = name

    def add_argument(self, name_of_arg, default="", help="", type=str):
        name_of_arg = name_of_arg.replace("-", '').lower()
        self.args_to_parse[name_of_arg] = {"default": default, "help": help, "type": type}

    @staticmethod
    def convert_snake(name):
        """ Converts CamelCase to snake_case """
        s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
        return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()

    def parse_args(self, config, top_level=True, is_dict=False, print_help=True):
        yaml_config = {}
        args_dict = {}
        args = {}

        if not is_dict:
            with open(config, 'r') as stream:
                data = yaml.load(stream)
                if data:
                    yaml_config = data
        else:
            yaml_config = config

        for parent in self.parents:
            args_obj = parent.parse_args(config, top_level=False, is_dict=is_dict)
            args_dict.update(args_obj.__dict__)

            if top_level:
                args[parent.name] = args_obj

        if top_level:
            if print_help:
                self.print_help(args)
            return args

        for argument in list(self.args_to_parse.keys()):
            if self.name in yaml_config and argument in yaml_config[self.name]:
                args_dict[argument] = self.convert_value_type(self.args_to_parse[argument]["type"],
                  os.environ.get(ArgumentParser.convert_snake(self.name).upper()+'_'+argument.upper(),yaml_config[self.name][argument]))
            else:
                args_dict[argument] = self.args_to_parse[argument]["default"]

        return ArgumentArgs(args_dict)


    def print_help(self, args):
        help = "-------------------------------------\n" \
               "Running service with this parameters:\n" \
               "Description: %s \n %s \n" \
               "-------------------------------------\n"

        dictionary = {}
        for key in list(args.keys()):
            dictionary[key] = args[key].__dict__

        help = help % (self.description, json.dumps(dictionary, indent=4))

        print(help)

    def convert_value_type(self, type, value):
        return type(value)
