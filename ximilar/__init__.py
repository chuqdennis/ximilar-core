from pkg_resources import declare_namespace

declare_namespace("ximilar")

__project__ = 'ximilar'
__version__ = "0.1.2"
