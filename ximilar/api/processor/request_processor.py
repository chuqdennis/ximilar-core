import abc

from ximilar.base.record_checker import RecordChecker


class RequestProcessor(RecordChecker):
    """
    This is a base class for JSON request processors.
    """

    @abc.abstractmethod
    def process_request(self, json_request):
        """
        Given a JSON record, this is the main method that should modify the given record.
        :param json_request: JSON object (map) as returned by JSON.parse
        :return: the modified record (request)
        :raise Exception if the processing fails
        """
        raise NotImplementedError
