import sys
import copy
import traceback

from ximilar.api.common_api import Request, Response
from ximilar.api.python_api import Record
from ximilar.api.processor.request_processor import RequestProcessor
from ximilar.base.config import ArgumentParser
from ximilar.base.logger import Logger
from ximilar.base.exception import DeepException, ERRCODE, ERRMSG
from ximilar.base.data_processor import DataProcessor
from ximilar.base.data_cleaner import DataCleaner


class GenericProcessor(RequestProcessor):
    """
    This is a class that takes a JSON request and it applies one or more DataProcessors on the encapsulated data records.
    """

    @staticmethod
    def get_arg_parser(name="GenericProcessor"):
        parser = ArgumentParser(name)
        parser.add_argument("--max_batch", help="The max size of batch(number of records) with which server can work",
                            type=int, default=2500)
        parser.add_argument("--settings", help="Default settings in json records which we want to copy to every record",
                            type=list, default=[Record.LANG, Record.COLORNAMES, Record.TAGGING_MODE, Record.MODE])
        return parser

    def __str__(self):
        return "GenericRequestProcessor"

    def __init__(self, args, data_processors, name="GenericProcessor"):
        """
        Check if the given processors are of type DataProcessor and store them.
        :param data_processors: iterable with objects of type DataProcessor
        :return: None
        """
        self.max_batch = args[name].max_batch
        self.settings = args[name].settings

        for processor in data_processors:
            if not isinstance(processor, DataProcessor):
                raise DeepException(ERRCODE.NOTINSTANCE, "GenericProcessor:" + ERRMSG[ERRCODE.NOTINSTANCE])
        self.processors = data_processors
        self.logger = Logger(args, 'GenericProcessor')

    def process_request(self, json_request):
        """
        Process JSON record by applying processor(data loader, ...) on json.
        For example data loader processor is called for loading data.
        :param json_request: Represents json data of request from client.
        :return: json data which will be further processed by another processor
                 or sent as response from server.
        :rtype: dict
        """
        json_request = self.extend_settings(json_request)
        json_request = self.extend_status(json_request)

        json_request[Request.RECORDS] = self.process_batch(json_request[Request.RECORDS])
        return json_request

    def extend_settings(self, json_request):
        """
        Adds a "_settings" object into each record of the request; the settings contain all parameters (except "records")
        :param json_request: a parsed JSON-like API request
        :return: modified request
        """
        self.logger.debug('Extending each record by settings')

        for i in range(len(json_request[Request.RECORDS])):
            if Record.SETTINGS not in json_request[Request.RECORDS][i]:
                json_request[Request.RECORDS][i][Record.SETTINGS] = {}

            for setting in self.settings:    
                # copy all params to _settings
                if setting in json_request:
                    json_request[Request.RECORDS][i][Record.SETTINGS][setting] = json_request[setting]

        self.logger.debug('Extending settings complete')
        return json_request

    def extend_status(self, json_request):
        """
        Adds a "status" object to each record of the request
        :param json_request: an API request that is supposed to contain "records"
        :return:
        """
        self.logger.debug('Extending each record by status information')

        for i in range(len(json_request[Request.RECORDS])):
            json_request[Request.RECORDS][i][Record.REC_STATUS] = {Response.STATUS_CODE: 200, Response.STATUS_TEXT: "OK"}
        return json_request

    def process_batch(self, data_records):
        """
        Given a json record with batch of samples, this method calls the processing.
        :param data_records:  json record
        :return: array of JSON records (dictionaries)
        :rtype: list of dictionaries
        """
        try:
            if len(data_records) > self.max_batch:
                raise DeepException(ERRCODE.EXTRACTION_ERROR,
                                    "GenericRequestProcessor: batch size exceeded number" + str(self.max_batch))

            bad_records = []
            sys.stdout.flush()

            for processor in self.processors:
                self.logger.debug('Processing ' + str(len(data_records)) +  ' records with ' + str(processor))
                to_remove = []

                for record in data_records:
                    # Checking precondition and if it fails remove the record from processing
                    if not processor.pre_condition(record):
                        # Inserting error status to record
                        record[Record.REC_STATUS][Response.STATUS_CODE] = 500
                        record[Record.REC_STATUS][Response.STATUS_TEXT] = processor.pre_condition_desc()

                        bad_records.append(copy.deepcopy(record))
                        to_remove.append(record)

                for remove_record in to_remove:
                    data_records.remove(remove_record)

                if len(data_records) > 0:
                    data_records = processor.process_records(data_records)

                self.logger.debug('Processing complete ' + str(processor))

            # If we are cleaning our data we need to clean also internal info from bad records
            if isinstance(self.processors[-1], DataCleaner):
                self.logger.debug('Cleaning records with error with ' + str(processor))
                bad_records = self.processors[-1].process_records(bad_records)

            data_records.extend(bad_records)
            return data_records
        except DeepException as e:
            self.logger.debug('Deep exception thrown during processing: ' + str(e) + traceback.format_exc(e))
            self.logger.sentry_exception(e)
            raise DeepException(e.code, e.msg)
        except Exception as e:
            self.logger.debug('Exception thrown during processing: ' + str(e) + traceback.format_exc(e))
            self.logger.sentry_exception(e)
            raise DeepException(ERRCODE.DEFAULT, str(e))

    def pre_condition(self, json_record):
        return Request.RECORDS in json_record

    def pre_condition_desc(self):
        return ERRMSG[ERRCODE.CONTRECORD] % Request.RECORDS

    def get_modified_fields(self):
        return [Request.RECORDS]
