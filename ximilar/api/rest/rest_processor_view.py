import json
import traceback

from flask import request, Response

from ximilar.api.common_api import Request, Record
from ximilar.api.common_api import Response as ApiResponse
from ximilar.api.processor import RequestProcessor
from ximilar.base.logger import Logger
from ximilar.base.exception import DeepException, ERRCODE


class RESTProcessorView(object):
    """
    This class represents entry point for handling request for our flask server.

    It contains processor which implements method process_record[s] for further
    processing of json_record/request.
    """

    def __init__(self, args, processors, name="RESTProcessorView"):
        """
        Initialize object with processors. Processors can be data loaders, data augmenters,
        celery processor and so on. Each processor inherits from DataProcessor class.

        :param processors: [DataProcessor]
        :return: None
        """
        # init logging
        self.logger = Logger(args, name)

        # Check that all variables are of the right type
        for m in processors:
            if not isinstance(m, RequestProcessor):
                raise Exception('the passed instance is not of type DataProcessor: ' + str(m))
        self.processors = processors
        from json import encoder
        encoder.FLOAT_REPR = lambda o: format(o, '.3f')

    def process_record(self):
        """
        Process record by calling all possible processors and return http response.
        :return: Flask.Response
        """
        request_record = None

        try:
            if request.json is None:
               raise Exception('Error when parsing request. Possible bad format and params of request.')

            if self.logger.log_debug:
                self.logger.debug("Processing request:" + json.dumps(request.json))
        
            request_record = request.json
            for p in self.processors:
                self.logger.debug('Processing with ' + str(p))
                if not p.pre_condition(request_record):
                    raise DeepException(ERRCODE.PRECONDITION, p.pre_condition_desc())
                request_record = p.process_request(request_record)
                self.logger.debug('Processed request ' + str(p))

            request_record[ApiResponse.STATUS] = self.check_records_status(request_record)
            if request_record[ApiResponse.STATUS][ApiResponse.STATUS_CODE] == ApiResponse.STATUS_CODE_OK:
                try:
                    self.logger.debug("Processing request completed:" + json.dumps(request.json) + ' result: ' + json.dumps(request_record))
                except TypeError:
                    self.logger.debug("Processing request completed:" + json.dumps(request.json) + ' result: ' + str(request_record))

            return Response(json.dumps(request_record), mimetype='application/json', status=200)
        except Exception as e:
            self.logger.debug('Exception: ' + str(e) + ' StackTrace: ' + traceback.format_exc())
            if request_record:
                try:
                    return self.handle_exception(json.dumps(request_record), str(e))
                except TypeError:
                    return self.handle_exception(str(request_record)[:200] + '...(shortened)', str(e))
            return self.handle_exception(json.dumps({}), str(e))

    def check_records_status(self, json_record):
        k = 0
        for i in range(len(json_record[Request.RECORDS])):
            if json_record[Request.RECORDS][i][Record.REC_STATUS][ApiResponse.STATUS_CODE] != ApiResponse.STATUS_CODE_OK:
                try:
                    self.logger.error("Error processing record: " + json.dumps(json_record[Request.RECORDS][i]))
                except TypeError:
                    self.logger.error("Error processing record: " + str(json_record[Request.RECORDS][i]))
                k += 1

        if k == len(json_record[Request.RECORDS]):
            try:
                self.logger.error("Error processing request: " + json.dumps(json_record))
            except TypeError:
                self.logger.error("Error processing request: " + str(json_record))
            return {ApiResponse.STATUS_CODE: ApiResponse.STATUS_CODE_ERROR,
                    ApiResponse.STATUS_TEXT: "ERROR"}

        if k > 0:
            return {ApiResponse.STATUS_CODE: ApiResponse.STATUS_CODE_MIXED_RESULT,
                    ApiResponse.STATUS_TEXT: "MIXED_RESULT"}

        return {ApiResponse.STATUS_CODE: ApiResponse.STATUS_CODE_OK, ApiResponse.STATUS_TEXT: "OK"}

    def handle_exception(self, json_dumped, error_txt):
        self.logger.sentry_exception()
        json_response = json.dumps({ApiResponse.STATUS: {ApiResponse.STATUS_CODE: ApiResponse.STATUS_CODE_ERROR,
                                                         ApiResponse.STATUS_TEXT: "ERROR",
                                                         ApiResponse.STATUS_ERROR: error_txt}})

        self.logger.error("Error processing request: " + json_dumped + ' with response ' + json_response)
        return Response(json_response, mimetype='application/json')
