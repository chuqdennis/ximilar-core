import json
import requests

from ximilar.api.processor import RequestProcessor
from ximilar.base.config import ArgumentParser
from ximilar.base.logger import Logger


class RESTClient(RequestProcessor):
    """
    A stub that just sends all requests to a specified server/method.
    """

    @staticmethod
    def get_arg_parser(name="RESTClient"):
        default_url = 'http://localhost:4005/v2/descriptor'
        parser = ArgumentParser(name=name)
        parser.add_argument("--server_url", default=default_url,
            help="http://host:port/method of a service to which this client connects, default: '" + default_url + "'")
        return parser

    def __init__(self, args, condition=None, modified_fields=None, name="RESTClient"):
        self.logger = Logger(args, 'RESTClient')
        self.logger.info('starting loggin RESTClient')

        self.url = args[name].server_url
        self.condition = condition
        self.modified_fields = modified_fields
        self.MAX_RETRIES = 2
        self.TIMEOUT = 100

    def call_rest(self, json_request):
        """
        Given a JSON record, this method actually connects to the REST service assuming that it expects a JSON and
        returns a modified JSON record
        :param json_request: JSON record as string
        :return:
        """
        # print(self.url)
        response = requests.post(self.url, json=json_request, timeout=self.TIMEOUT)
        if response.status_code not in [requests.codes.ok, requests.codes.created, requests.codes.partial, requests.codes.accepted ]:
            raise Exception('error occurred when processing record "' + json.dumps(json_request)[0:200] + '...(shortened)"; response: '
                            + str(response) + '\n')
        return response.content

    def process_request(self, json_request):
        response = None
        error = None
        for i in range(self.MAX_RETRIES):
            try:
                response = self.call_rest(json_request)
                return json.loads(response.decode('utf-8'))
            except Exception as e:
                self.logger.info('processing exception: retry: ' + str(i) + ": " + str(e) + "; response: " + str(response))
                error = e
        else:
            self.logger.error('error occurred when processing record: "' + json.dumps(json_request)[0:200] + '...(shortened)"; response: ' +
                              str(response) + "; error: " + str(error))
            raise Exception(
                'error occurred when processing record "' + json.dumps(
                    json_request)[0:200] + '...(shortened)": max retries (' + str(self.MAX_RETRIES) + ') exceeded!')

    def pre_condition(self, json_record):
        return self.condition(json_record) if self.condition else True

    def pre_condition_desc(self):
        return 'this condition is checked: ' + self.condition if self.condition else 'no condition known'

    def get_modified_fields(self):
        return self.modified_fields
