class Status(object):
    SUCCESS = 200
    PARTIAL = 210
    ERROR = 500
    WRONG_REQUEST = 400

    dictionary = {
        SUCCESS: 'success',
        PARTIAL: 'partial success',
        ERROR: 'error',
        WRONG_REQUEST: 'wrong request data'
    }

    @staticmethod
    def get_string_status(int_status):
        return Status.dictionary[int_status]