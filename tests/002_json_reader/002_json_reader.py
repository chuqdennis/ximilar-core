import base64
import json

import cv2
import numpy as np
import unittest

import sys

from ximilar.api.python_api import Record
from ximilar.misc.json_data import read_json_file_iterator
from ximilar.processor.data import ImgDataLoader
from ximilar.misc.config import ArgumentParser


class TestJSONReader(unittest.TestCase):
    """
    Test suite for testing reading in the JSON records from a file
    """

    @classmethod
    def setUpClass(self):
        """ get_some_resource() is slow, to avoid calling it for each test use setUpClass()
            and store the result as class variable
        """
        super(TestJSONReader, self).setUpClass()

        parser = ArgumentParser(name="json_reader", description="A TestJSONReader parser")
        parser.add_argument("--file", help="with with JSON lines to read in and check")
        parser.add_argument("--print_out", type=bool, help="if true, the data is also printed out", default=False)
        args = parser.parse_args("config.yaml")

        if not args.file or args.file is "":
            print("--file must be specified")
            exit()

        self.json_reader = read_json_file_iterator(args.file)
        self.print_out = args.print_out

    def test_read_data(self):
        counter = 0
        for line in self.json_reader:
            line = self.modify(line)
            if self.print_out:
                json.dump(line, sys.stdout)
                print("")
            counter += 1
        sys.stderr.writelines("successfully read " + str(counter) + " JSON lines with records")

    def modify(self, json_record):
        # if json_record[Record.DOMINANT_COLORS]:
        #     json_record[Record.DOMINANT_COLORS][Record.PERCENTAGES] = [x / 100 for x in json_record[Record.DOMINANT_COLORS][Record.PERCENTAGES]]
        return json_record

if __name__ == '__main__':
    unittest.main()
